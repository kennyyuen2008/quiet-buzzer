This project contains both server side and client side. Server side is built with Express, MySQL, Socket.io. Client side is created with `create-react-app`.

## Available Scripts

In the project directory, you can run:

### `npm run server`

Runs the server on [http://localhost:5000](http://localhost:5000).  
You will also see any lint errors in the console.

### `npm run client`

Runs the react client on [http://localhost:3000](http://localhost:3000).  
You will also see any lint errors in the console.

### `npm run dev`

Launches both client and server with concurrently.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!