import * as dotenv from "dotenv";
import express from "express";
import { createConnection } from "mysql";
import { Server } from "socket.io";
import {
   cancelTimer,
   clearTimer,
   createTimer,
   getAllTimers,
   pauseResumeTimer,
   setNotify,
} from "./lib/timer";
import XbeeDecoder from "./lib/xbee/xbee_decoder";

dotenv.config();

// http server, socket io
const app = express();
const server = require("http")
   .Server(app)
   .listen(process.env.SERVER_PORT, () => {
      console.log("open server!");
   });
const io = new Server(server);

// Database
const db = createConnection({
   host: process.env.DATABASE_HOST,
   user: process.env.DATABASE_USER,
   password: process.env.DATABASE_PASSWORD,
   port: process.env.DATABASE_PORT,
   database: process.env.DATABASE_DBNAME,
});

// Xbee Get data loop && Decoder
db.connect((err) => {
   if (err) console.log(err);
   console.log("Connected to Database");
   // Listen to every event after Server connected

   // Listen to serial port
   const decoder = new XbeeDecoder(
      process.env.XBEE_PORT,
      process.env.XBEE_BAUD
   );
   decoder.start();

   decoder.on(process.env.XBEE_BTN_5MIN, (seat) => {
      createTimer(db, io, seat, 5);
   });
   decoder.on(process.env.XBEE_BTN_10MIN, (seat) => {
      createTimer(db, io, seat, 10 * 60);
   });
   decoder.on(process.env.XBEE_BTN_15MIN, (seat) => {
      createTimer(db, io, seat, 15 * 60);
   });
   decoder.on(process.env.XBEE_BTN_20MIN, (seat) => {
      createTimer(db, io, seat, 20 * 60);
   });
   decoder.on(process.env.XBEE_BTN_CANCEL, (seat) => {
      clearTimer(db, io, seat);
   });
   decoder.on(process.env.XBEE_BTN_PAUSERESUME, (seat) => {
      pauseResumeTimer(db, io, seat);
   });

   io.on("connection", (socket) => {
      // Show on console after connection
      console.log("success connect!");

      // Let client know current non-finished countdown
      getAllTimers(db, io);

      //on
      socket.on("notifyTimer", (message) => {
         setNotify(db, io, message);
      });

      socket.on("cancelTimer", (message) => {
         cancelTimer(db, io, message);
      });

      socket.on("createTimer", (message) => {
         createTimer(db, io, message.seat, message.limit * 60);
      });

      socket.on("pauseTimer", (message) => {
         pauseResumeTimer(db, io, message);
      });

      socket.on("clearTimer", (message) => {
         clearTimer(db, io, message);
      });
   });
});
