// Get Current timestamp in seconds
export const getCurrentTime = () => Math.floor(Date.now() / 1000);
