import { getCurrentTime } from "../helper";

export const getAllTimers = (db, io) => {
   db.query("SELECT * FROM tbl_to_count_down", (err, rows) => {
      if (err) throw err;
      io.sockets.emit("showrows", rows);
   });
};

// Start a new Timer
export const createTimer = (db, io, seat, limit_in_seconds) => {
   let now = getCurrentTime();

   // check if seat exist
   db.query(
      ```
      SELECT COUNT(*) AS total 
      FROM tbl_to_count_down 
      WHERE seat = ?
      ```,
      [seat],
      (err, results) => {
         if (err) throw err;

         console.log(
            `Existed records of ${seat} seats: ${results[0]["total"]}`
         );

         if (results[0]["total"] == 0) {
            // post data to DB
            db.query(
               ```
               INSERT INTO tbl_to_count_down (seat, total_time_limit, start_time, remaining_time, pause) 
               VALUES (?,?,?,?,?)
               ```,
               [seat, limit_in_seconds, now, limit_in_seconds, false],
               (err) => {
                  if (err) throw err;

                  console.log(
                     `Created timer: ${seat},${limit_in_seconds},${now},${limit_in_seconds}`
                  );
                  getAllTimers(db, io);
               }
            );
         }
      }
   );
};

// Cancel a countdown
export const cancelTimer = (db, io, seat) => {
   db.query(
      ```
      DELETE FROM tbl_to_count_down WHERE seat = ?
      ```,
      [seat],
      (err) => {
         if (err) throw err;

         console.log(`Cancelled Timer: ${seat}`);
         io.sockets.emit("CancelledTimer", seat);
      }
   );
};

// Clear timer then trigger clear notice
export const clearTimer = (db, io, seat) => {
   db.query(
      ```
      SELECT COUNT(*) AS total 
      FROM tbl_to_count_down 
      WHERE seat = ?
      ```,
      [seat],
      (err, results) => {
         if (err) throw err;

         console.log(
            `Existed records of ${seat} seats: ${results[0]["total"]}`
         );

         if (results[0]["total"]) {
            db.query(
               ```DELETE FROM tbl_to_count_down WHERE seat = ?`,
               [seat],
               (err, result) => {
                  if (err) throw err;
                  console.log(`Deleted Timer: ${seat}`);
                  getAllTimers(db, io);
               }
            );
         }
      }
   );
};

export const setNotify = (db, io, seat) => {
   db.query(
      ```SELECT COUNT(*) AS total 
      FROM tbl_to_count_down 
      WHERE seat = ?`,
      [seat],
      (err, results) => {
         if (err) throw err;

         console.log(
            `Existed records of ${seat} seats: ${results[0]["total"]}`
         );

         if (results[0]["total"]) {
            db.query(
               ```UPDATE tbl_to_count_down 
               SET notify = true 
               WHERE seat = ?
               ```,
               [seat],
               (err, result) => {
                  if (err) throw err;
                  console.log(`Notified Timer: ${seat}`);
                  getAllTimers(db, io);
               }
            );
         }
      }
   );
};

/*  
Pause/resume a countdown
=====================
To Resume:
---------------------
	start: 1200
	timelimit: 10
	remain: 5

	resume now 1215
	start: 1215
	remain: 5
	pause: false

	start = current
	pause = false
======================
To Pause:
----------------------
	start: 1200
	timelimit: 10
	remain: 10
	
	pause now 1205
	remain: 5
	
	remain = remain - (current - start)
	pause = true
*/
export const pauseResumeTimer = (db, io, seat) => {
   let sql = ``;
   db.query(
      ```
      SELECT pause, remaining_time, start_time 
      FROM tbl_to_count_down 
      WHERE seat = ?
      ```,
      [seat],
      (err, results) => {
         if (err) throw err;

         if (results[0] == undefined) {
            console.log(`Records not found: ${seat}`);
            return;
         }

         if (results[0].pause) {
            db.query(
               ```
               UPDATE tbl_to_count_down 
               SET start_time = ?, pause = false 
               WHERE seat = ?
               ```,
               [getCurrentTime(), seat],
               (err) => {
                  if (err) throw err;

                  console.log(`Resumed Timer: ${seat}`);

                  getAllTimers(db, io);
               }
            );
            return;
         }

         if (results[0].remaining_time && results[0].start_time) {
            db.query(
               ```
               UPDATE tbl_to_count_down 
               SET remaining_time = ?, pause = true 
               WHERE seat = ?
               ```,
               [
                  results[0].remainingtime -
                     (getCurrentTime() - results[0].starttime),
                  seat,
               ],
               (err) => {
                  if (err) throw err;

                  console.log(`Paused Timer: ${seat}`);

                  getAllTimers(db, io);
               }
            );
            return;
         }
      }
   );
};
