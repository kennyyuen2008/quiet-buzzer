import EventEmitter from "events";
import XbeeConnector from "./xbee_connector";

export default class XbeeDecoder extends EventEmitter {
   constructor(port, baud) {
      super();
      this.Connector = new XbeeConnector(port, baud);
   }
   decode(data) {
      const segments = data.split("seat:B");
      return { seat: segments[0], button: segments[1] };
   }
   start() {
      const _this = this;
      this.Connector.listen();
      this.Connector.on("data", (data) => {
         let record = this.decode(data);
         _this.emit(`button${record.button}`, record.seat);
      });
   }
}
